# dotfiles

This is what my i3 looks like
![i3 screenshot](https://raw.github.com/KostasKyriakou/dotfiles/master/i3wm.png)

## neovim config
Neovim config based on coc.nvim and good support for languages like C/C++ and rust. Also, vimwiki is there because it's great.

## zsh config
Zsh config based on oh-my-zsh. Includes plugins like autojump, syntax-highlight, git and aliases I find useful and you may do so too.

## i3 config
Config is always under construction.

## polybar config
Quite simple bar.(screenshot above)

Note: Make scripts executable.

## rofi config
Also simple rofi config much like pressing mod in many DEs. Arc theme.

## alacritty config
Beware the font size. Srcery theme and a little transparency.

## vifm config
Picture preview in vifm. Make scripts executable.

# Copying everything to your home
You can `./confie install dotfiles` to install everything. For more information visit [confie](https://gitlab.com/langhops/confie).
